# Genesys Cloud Common Archy Flows

The common flows contained within this repository can be imported to a Genesys Cloud org using Archy to accelerate development.

## Pre-requisites

In order to publish a flow with Archy, you must configure Archy on your workstation to authenticate to the desired org. [Click here to view installation and setup steps.](https://developer.mypurecloud.com/devapps/archy/)

## Inbound Flows

### Cache Example (`CacheExampleFlow.yaml`)

This flow uses a data table to cache data action results from the [Get On Queue Agent Counts](https://bitbucket.org/cloud-digital/genesys-cloud-data-actions/src/master/Get-On-Queue-Agent-Counts.json) data action, providing greater scalability for our largest customers.

#### Usage

Instructions for using this flow are available in [this Developer Center blog post](https://developer.mypurecloud.com/blog/2021-02-03-Caching-in-flows/).

### Callback Scheduler (`CallbackScheduler.yaml`)

This flow uses data actions and schedules to provide callers with callback scheduling options, allowing them to schedule a callback in the IVR based on the number of callbacks already in queue. The following data actions from the data actions repository are used in this flow:

- [Create Callback Extended](https://bitbucket.org/cloud-digital/genesys-cloud-data-actions/src/master/Create-Callback-Extended.json)
- [Disconnect Callback Conversation](https://bitbucket.org/cloud-digital/genesys-cloud-data-actions/src/master/Disconnect-Callback-Conversation.json)
- [Get Existing Scheduled Callback](https://bitbucket.org/cloud-digital/genesys-cloud-data-actions/src/master/Get-Existing-Scheduled-Callback.json)
- [Get Interactions in Queue](https://bitbucket.org/cloud-digital/genesys-cloud-data-actions/src/master/Get-Interactions-In-Queue.json)
- [Get Number of Completed Callbacks](https://bitbucket.org/cloud-digital/genesys-cloud-data-actions/src/master/Get-Number-of-Completed-Callbacks.json)

Callers can schedule a callback for the current or next nine intervals. It splits the day into two intervals and a limit can be set for the maximum number of callbacks created in the current interval. There is no API that can provide callback count by scheduled date, so we can’t enforce a limit for future intervals. This shortcoming is generally acceptable, since most callbacks are created for the current interval anyway. The callback scheduler accounts for current callbacks in-queue (connected and waiting) in addition to callbacks answered from the interval start. The flow is pre-configured for two intervals, starting at 8:00 AM and 12:30 PM Central Daylight Time, with a maximum limit of 250 callback interactions.

This solution differs from the [automated callback blueprint](https://developer.genesys.cloud/blueprints/automated-callback-blueprint/) by allowing the caller to select a desired callback time, at which point it will be offered to queue for an agent to handle. In contrast, the blueprint uses outbound dialing to call back the customer ASAP, with an IVR front-end on the callback.

#### Usage

1. Ensure the above data actions are imported and published.
2. Create a schedule group named `Callback Schedules` to define available callback times.
3. On line 269, set the value of `Flow.CBQueue` to the name of the queue that callbacks should be created in.
4. If `America/Chicago` is not the desired time zone, update the `Flow.CDTOffset` variable expression on line 293 to adjust the UTC offset.

### SMS Survey (`SMSSurveyFlow.yaml`)

This flow uses the [Send SMS](https://bitbucket.org/cloud-digital/genesys-cloud-data-actions/src/master/Send-SMS.json) data action to send survey invites via SMS rather than via email. This is useful for scenarios where the only known customer data is their ANI.

#### Usage

1. Ensure the customer's org has a valid [SMS number purchased](https://help.mypurecloud.com/articles/purchase-sms-numbers/).
2. Ensure the [Send SMS](https://bitbucket.org/cloud-digital/genesys-cloud-data-actions/src/master/Send-SMS.json) data action is published.
3. Follow the notes in the `SMSSurveyFlow.yaml` file indicating what customer-specific items must be changed prior to publishing the flow.
